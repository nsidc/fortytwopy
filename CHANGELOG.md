# v2.0.1 (4/20/2018)

* Fix conda package metadata: link, license, description.

# v2.0.0 (4/20/2018)

* Simplify releases. We no longer use the anaconda.org dev label, and
  we version and release on master the same as with other `fortytwo`
  projects.
* Change license to MIT.
* Add and switch to bumpversion
* Update README with better instructions

# v1.0.5 (2017-09-25)

Use workflows

# v1.0.2 (7/19/2017)

Add a `__version__` attribute to the fortytwopy package.

# v1.0.0 (4/25/2017)

Initial release of the package using CircleCI.
