from setuptools import setup, find_packages

setup(name='fortytwopy',
      version='2.0.1',
      description='A test package: figure out how to build & publish using BitBucket Pipelines.',
      url='git@bitbucket.org/kbeamnsidc/fortytwopy.git',
      author='NSIDC Development Team',
      author_email='programmers@nsidc.org',
      license='GPLv3',
      packages=find_packages(exclude=('tasks',)))
