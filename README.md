FortyTwoPy
---

A conda package to demonstrate how to test, build and publish
to [Anaconda.org](https://anaconda.org/)
using [CircleCI](https://circleci.com/).

[![CircleCI](https://circleci.com/bb/nsidc/fortytwopy.svg?style=svg)](https://circleci.com/bb/nsidc/fortytwopy)

[![Anaconda-Server Badge](https://anaconda.org/nsidc/fortytwopy/badges/version.svg)](https://anaconda.org/nsidc/fortytwopy)
[![Anaconda-Server Badge](https://anaconda.org/nsidc/fortytwopy/badges/license.svg)](https://anaconda.org/nsidc/fortytwopy)
[![Anaconda-Server Badge](https://anaconda.org/nsidc/fortytwopy/badges/downloads.svg)](https://anaconda.org/nsidc/fortytwopy)
[![Anaconda-Server Badge](https://anaconda.org/nsidc/fortytwopy/badges/installer/conda.svg)](https://conda.anaconda.org/nsidc)

Prerequisites
---

* [Miniconda3](https://conda.io/miniconda.html)

Development
---

Install dependencies:

    $ conda env create -f environment.yml
    $ source activate fortytwopy

Workflow
---

TL;DR:  Use
[GitHub Flow](https://guides.github.com/introduction/flow/index.html).

In more detail:

1. Create a feature branch.
2. Create and push commits on that branch.
3. The feature branch will get built on CircleCI with each push.
4. Update the CHANGELOG with description of changes.
5. Create a Pull Request on BitBucket.
6. When the feature PR is merged, master will get built on CircleCI.

Releasing
---

1. Update the CHANGELOG to list the new version.
2. Add files and commit

        $ git add CHANGELOG.md ...
        $ git commit -m "Release v.X.Y.Z"

3. Bump the version to the desired level:

        $ bumpversion (major|minor|patch)

4. Push

        $ git push origin master --tags

CircleCI will build the conda package and publish it to anaconda.org.

Installing
---
To install and use it in another project:

    $ conda install fortytwopy

License
---

Copyright 2018 National Snow and Ice Datacenter (NSIDC)
<programmers@nsidc.org>

This code is licensed and distributed under the terms of the MIT
License (see LICENSE).
